import BlogItemCard from "../../components/BlogItemCard/BlogItemCard";
import Button from "../../components/Button/Button";
import Countup from "../../components/Countup/Countup";
import Feedback from "../../components/Feedback/Feedback";
import "./Home.css";

const Home = () => {
  const runCallback = (cb) => {
    return cb();
  };

  return (
    <section className="page">
      {/* hero section  */}
      <section
        className="hero-section"
        style={{ background: "var(--yellow)", height: "60vh" }}
      >
        <div className="container h-100 d-flex justify-content-center align-items-center">
          <div className="d-flex flex-column justify-content-center align-items-center">
            <h1
              className="text-center fw-bolder"
              style={{
                color: "var(--blue)",
                fontSize: "2.5em",
              }}
            >
              Vous<span style={{ color: "#FFF" }}> êtes votre meilleur </span>
              investissement
            </h1>
            <p className="text-center my-3 fw-bold" style={{ color: "#FFF" }}>
              Le meilleur investissement que vous pouvez faire est d’investir en
              vous. Diversifiez votre savoir-faire en vous formant dans des
              secteurs porteurs, à long terme
            </p>

            <Button background="var(--pink)">Formez-vous</Button>
          </div>
        </div>
      </section>

      {/* home carousel  */}
      <section
        className="carousel-wrapper py-5"
        style={{
          background: "var(--light-pink)",
          // minHeight: "40vh",
        }}
      >
        <Feedback />
      </section>

      {/* key numbers */}
      <section className="key-numbers py-5">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4 my-5">
              <Countup title="Abonnés" max="49000" />
            </div>
            <div className="col-12 col-md-4 my-5">
              <Countup title="Participants" max="3426" />
            </div>
            <div className="col-12 col-md-4 my-5">
              <Countup title="Conférences" max="38" />
            </div>
          </div>
        </div>
      </section>

      {/* blog section  */}
      <section
        className="blog-section py-5"
        style={{ background: "var(--blue-lighter)" }}
      >
        <div className="container">
          <h2
            className="text-center fw-bold py-2"
            style={{ color: "var(--blue-darker)" }}
          >
            Explorez notre vision
          </h2>

          <div className="row py-5">
            <div className="col-12 col-md-6">
              <BlogItemCard />
            </div>

            <div className="col-12 col-md-6">
              <BlogItemCard />
            </div>

            <div className="col-12 col-md-6">
              <BlogItemCard />
            </div>
          </div>

          <div style={{ width: "fit-content", margin: "auto" }}>
            <Button background="var(--blue-darker)">Load More</Button>
          </div>
        </div>
      </section>

      {/* branch list  */}
      <section className="branch-list py-5">
        <div className="container">
          <h2
            className="text-center fw-bold py-2"
            style={{ color: "var(--blue-darker)" }}
          >
            Des StartUps Made in Cameroon by StartUp Academy
          </h2>
          <div className="row">
            {runCallback(() => {
              const row = [];
              for (var i = 0; i < 4; i++) {
                row.push(
                  <>
                    <div className="col-12 col-md-3">
                      <a href="f">
                        <img
                          src="/statics/images/medecin-2.0.jpg"
                          alt="statup group company"
                        />
                      </a>
                    </div>
                    <div className="col-12 col-md-3">
                      <a href="f">
                        <img
                          src="/statics/images/startup-color.png"
                          alt="statup group company"
                        />
                      </a>
                    </div>
                    <div className="col-12 col-md-3">
                      <a href="f">
                        <img
                          src="/statics/images/sur-mesure.png"
                          alt="statup group company"
                        />
                      </a>
                    </div>
                    <div className="col-12 col-md-3">
                      <a href="f">
                        <img
                          src="/statics/images/startup-cosmetic.png"
                          alt="statup group company"
                        />
                      </a>
                    </div>
                  </>
                );
              }
              return row;
            })}
          </div>
        </div>
      </section>
    </section>
  );
};

export default Home;
