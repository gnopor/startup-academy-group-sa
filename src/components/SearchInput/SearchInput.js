import { useRef } from "react";
import SearchIcon from "../Icon/SearchIcon";
import "./SearchInput.css";

const SearchInput = () => {
  const searchInputRef = useRef();
  const searchFormRef = useRef();

  const handleSeachDisplay = (is_close) => {
    const class_name = "show-form";
    const action = is_close ? "add" : "remove";
    searchInputRef.current.classList[action](class_name);
    searchFormRef.current.classList[action](class_name);
  };

  return (
    <div className="search-input-wrapper h-100 d-flex align-items-center">
      <span
        onClick={() => handleSeachDisplay(true)}
        style={{ cursor: "pointer" }}
      >
        <SearchIcon />
      </span>

      <div className="search-input" ref={searchInputRef}>
        <form
          className="search-form container py-3 d-flex align-items-center"
          ref={searchFormRef}
        >
          <span>
            <SearchIcon />
          </span>
          <input
            className="flex-grow-1 mx-3 border-0"
            type="text"
            placeholder="Type Your Search..."
          />
          <span
            onClick={() => handleSeachDisplay(false)}
            className="close-search-trigger rounded-circle d-flex justify-content-center align-items-center"
            style={{ cursor: "pointer" }}
          >
            X
          </span>
        </form>
      </div>
    </div>
  );
};

export default SearchInput;
