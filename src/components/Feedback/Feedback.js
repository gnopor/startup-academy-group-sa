import "./Feedback.css";

const Feedback = () => {
  return (
    <div>
      <div className="d-flex flex-column justify-content-center align-items-center">
        <h2 className="fw-bold text-center">
          Ils ont vécu des expériences édifiantes
        </h2>

        <div
          id="home-carousel"
          className="carousel slide"
          data-bs-ride="carousel"
          style={{ width: "100vw" }}
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <article className="container p-5 d-flex flex-column justify-content-center align-items-center ">
                <span style={{ fontFamily: "icomoon", fontSize: "3em" }}>
                  &#xe9ae;
                </span>
                <p className="text-center py-1">
                  Une approche du réveil de l'Afrique par le développement des
                  initiatives locales à travers les Start-up promues par des
                  jeunes... Que du bon!
                </p>
                <h5 className="fw-bold">DJOUMOU Mariane</h5>
              </article>
            </div>

            <div className="carousel-item">
              <article className="container p-5 d-flex flex-column justify-content-center align-items-center ">
                <span style={{ fontFamily: "icomoon", fontSize: "3em" }}>
                  &#xe9ae;
                </span>
                <p className="text-center py-1">
                  Une approche du réveil de l'Afrique par le développement des
                  initiatives locales à travers les Start-up promues par des
                  jeunes... Que du bon!
                </p>
                <h5 className="fw-bold">FOTSING Charle</h5>
              </article>
            </div>
          </div>

          {/* carousel indicators  */}
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#home-carousel"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
            ></button>
            <button
              type="button"
              data-bs-target="#home-carousel"
              data-bs-slide-to="1"
            ></button>
          </div>

          {/* carousel control */}
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#home-carousel"
            data-bs-slide="prev"
          >
            <span className="custom-control-icon" aria-hidden="false">
              &#xe908;
            </span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#home-carousel"
            data-bs-slide="next"
          >
            <span className="custom-control-icon" aria-hidden="false">
              &#xe90a;
            </span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Feedback;
