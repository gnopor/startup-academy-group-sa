import "./InputField.css";

const InputField = ({ placeholder, ref }) => {
  return (
    <input
      className="w-100 py-2 bg-transparent custom-input"
      type="text"
      placeholder={placeholder}
      ref={ref}
    />
  );
};

export default InputField;
