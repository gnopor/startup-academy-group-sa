import { Link } from "react-router-dom";
import Address from "../Address/Address";
import Button from "../Button/Button";
import InputField from "../InputField/InputField";
import "./Footer.css";

const Footer = () => {
  return (
    <footer className="fw-bold">
      <section
        className="footer1 px-5"
        style={{ background: "var(--blue-light)", color: "var(--light-gray)" }}
      >
        <div className="container ">
          <div className="row">
            {/* locations  */}
            <div className="col-12 col-md-4 py-5">
              <Address
                city="Yaoundé"
                location="Carrefour HYSACAM, Immeuble StartUp Academy, 4e étage"
                phone_number="654 306 227"
              />
              <Address
                city="Douala"
                location="Ancien Number One, près de la douche, AKWA (à côté de
                    l'immeuble ELISSAM MOTORS)"
                phone_number="694 711 901"
              />
            </div>

            {/* site map  */}
            <div className="col-12 col-md-4 py-5 site-map">
              <div className="d-flex flex-column h-100">
                <h4 style={{ color: "#FFF" }} className="fw-bolder">
                  Plan du site
                </h4>
                <span>Devenir Membre</span>
                <span>Formation</span>
                <span>Prestation</span>
                <span>Boutique</span>
                <span>Formations DVD</span>
                <span>Livres</span>
                <span>Blog</span>
                <span>Qui sommes-nous</span>
                <span>Contactez-nous</span>
              </div>
            </div>

            {/* news letter form */}
            <div className="col-12 col-md-4 py-5">
              <div>
                <h4 style={{ color: "#FFF" }} className="fw-bolder text-center">
                  Newsletter
                </h4>
                <p className="text-center">
                  Vous voulez être parmi les premiers à être informés des
                  nouveautés à la StartUp Academy ? Abonnez-vous à notre
                  Newsletter
                </p>

                <form>
                  <div className="py-3">
                    <InputField placeholder="Entrez votre nom" />
                  </div>
                  <div className="py-3">
                    <InputField placeholder="Entrez votre email" />
                  </div>

                  <div className="py-3">
                    <Button>Je m'abonne</Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="footer2 px-5">
        <div className="container">
          <div className="row py-5">
            <div className="col-12 col-lg-4">
              <div className="m-auto" style={{ width: "fit-content" }}>
                <Link to="">
                  <img
                    src="/statics/images/StartUp-Academy-New-Logo-3-light-1.png"
                    alt="startup academy logo 2"
                  />
                </Link>
              </div>
            </div>
            <div className="col-12 col-lg-4">
              <span
                className="d-block text-center py-4"
                style={{ fontSize: "0.8em" }}
              >
                © Copyrights {new Date().getFullYear()} START-UP ACADEMY
              </span>
            </div>
            <div className="col-12 col-lg-4 d-flex aling-items-center">
              <div className="m-auto d-flex justify-content-center align-items-center social-network">
                <span style={{ fontSize: "0.8em" }}>Suivez-nous:</span>
                <div style={{ fontFamily: "icomoon" }}>
                  <span>
                    <a href="/">&#xeb28;</a>
                  </span>
                  <span>
                    <a href="/">&#xeb49;</a>
                  </span>
                  <span>
                    <a href="/">&#xebd8;</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </footer>
  );
};

export default Footer;
