import { Link } from "react-router-dom";
import "./BlogItemCard.css";

const BlogItemCard = () => {
  return (
    <article className="blog-item-card" style={{ maxWidth: "100%" }}>
      <div className="picture">
        <Link to="">
          <img
            style={{ maxWidth: "100%" }}
            src="/statics/images/MG_9905-scaled.jpg"
            alt="blog item card"
            title="StartUP Colors, 1 an déjà !"
          />
        </Link>
      </div>

      <h4 className="fw-bold my-5" style={{ color: "var(--blue-darker)" }}>
        <Link to="">StartUP Colors, 1 an déjà !</Link>
      </h4>

      <div className="blog-item-card-footer  my-5">
        <Link to="">
          <span className="fw-bold">Read More</span>
          <div
            className="icon mx-2 p-3 rounded-circle"
            style={{
              fontFamily: "icomoon",
            }}
          >
            &#xeb83;
          </div>
        </Link>
      </div>
    </article>
  );
};

export default BlogItemCard;
