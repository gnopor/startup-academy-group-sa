import { useEffect, useRef, useState } from "react";
import "./MenuIcon.css";

const MenuIcon = ({ height, width, fill, color }) => {
  const menuIconRef = useRef();
  const [lineStyle, setLineStyle] = useState({});

  useEffect(() => {
    setLineStyle({ border: `2px solid ${color}` });
  }, [color, setLineStyle]);

  const handleHoverEffect = () => {
    const lines = menuIconRef.current.querySelectorAll(".line");

    const show = (element) => {
      setTimeout(() => {
        element.classList.remove("hide");
      }, 500);
    };

    const hide = (element, i) => {
      setTimeout(() => {
        element.classList.add("hide");
        show(element);
      }, i * 250);
    };

    let i = 0;
    while (i < lines.length) {
      const element = lines[i];
      hide(element, i);
      i++;
    }
  };

  return (
    <span
      className="menu-icon p-2 rounded-circle d-flex flex-column justify-content-center"
      style={{ height, width, background: fill, cursor: "pointer" }}
      onMouseEnter={handleHoverEffect}
      ref={menuIconRef}
    >
      <span className="line" style={lineStyle}></span>
      <span className="line" style={lineStyle}></span>
      <span className="line" style={lineStyle}></span>
    </span>
  );
};

MenuIcon.defaultProps = {
  height: "50px",
  width: "50px",
  fill: "var(--blue)",
  color: "white",
};

export default MenuIcon;
