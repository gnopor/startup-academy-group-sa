import { useCallback, useEffect, useState } from "react";

const Countup = ({ title, max, second }) => {
  const [number, setNumber] = useState(0);

  const handleCount = () => {
    let i = 0;
    const iteration = 8;
    const interval_time = 1000 / +iteration;

    const intervale = setInterval(() => {
      i += max / +iteration;
      setNumber(Math.round(i));
      if (i >= max) {
        setNumber(max);
        clearInterval(intervale);
      }
    }, +interval_time);
  };

  const startCountUp = useCallback(handleCount, [max]);

  useEffect(() => {
    startCountUp();
  }, [startCountUp]);

  return (
    <article
      className="custom-countup fw-bold px-5 py-4 d-flex flex-column"
      style={{
        width: "100%",
        minHeight: "200px",
        boxShadow: " 0px 10px 3px 0px rgba(51, 51, 51, 0.1)",
      }}
    >
      <span style={{ fontSize: "0.8em" }}>{title}</span>
      <span
        className="my-2"
        style={{ color: "var(--yellow)", fontSize: "3.5em" }}
      >
        {number}
      </span>
    </article>
  );
};

Countup.defaultProps = {
  second: 2,
};

export default Countup;
