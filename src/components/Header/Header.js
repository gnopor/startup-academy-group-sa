import { useRef } from "react";
import { Link } from "react-router-dom";
import CartIcon from "../Icon/CartIcon";
import MenuIcon from "../Icon/MenuIcon/MenuIcon";
import NavItem from "../NavItem/NavItem";
import SearchInput from "../SearchInput/SearchInput";
import "./Header.css";

const Header = () => {
  const sidebarRef = useRef();
  const links = [
    { title: "Devenir\u00a0Membre", path: "", subs: [] },
    {
      title: "Services",
      path: "",
      subs: [
        { title: "Formations", path: "" },
        { title: "Prestations", path: "" },
      ],
    },
    {
      title: "Boutique",
      path: "",
      subs: [
        { title: "DVD", path: "" },
        { title: "Livres", path: "" },
      ],
    },
    {
      title: "A\u00a0Propos",
      path: "",
      subs: [
        { title: "Qui sommes-nous", path: "" },
        { title: "Contactez-nous", path: "" },
      ],
    },
  ];

  const handleShowSideBar = (event) => {
    sidebarRef.current.classList.toggle("show");
  };

  const handleShowSideBarSubMenu = (event) => {
    const submenu_item = document.querySelector("article.show");
    const current_submenu_item = event.target.parentElement.parentElement;

    if (current_submenu_item.isSameNode(submenu_item)) {
      current_submenu_item.classList.toggle("show");
    } else {
      submenu_item && submenu_item.classList.remove("show");
      current_submenu_item.classList.add("show");
    }
  };

  return (
    <header style={{ height: "100px" }} className="mw-100">
      <nav className="container mx-auto h-100">
        <ul className="d-flex justify-content-between align-items-center h-100">
          {/* logo  */}
          <li>
            <NavItem path="/">
              <img
                src="/statics/images/StartUp-Academy-New-Logo-5-icone.jpg"
                alt="logo"
                height="43px"
                width="153.2px"
              />
            </NavItem>
          </li>

          {/* dropdown menus  */}
          <li>
            <ul className="d-none d-lg-flex justify-content-between align-items-center">
              {links.map((link) => (
                <li key={link.title}>
                  <NavItem
                    children={link.title}
                    subs={link.subs}
                    with_sub={!!link.subs.length}
                    path={link.path}
                  />
                </li>
              ))}
            </ul>
          </li>

          {/* search and cart */}
          <li>
            <ul className="d-none d-lg-flex">
              <li className="mx-2">
                <SearchInput />
              </li>
              <li className="mx-2 d-flex align-items-center">
                <span>
                  <CartIcon />
                </span>
              </li>
            </ul>
          </li>

          {/* sideBar trigger*/}
          <li className="d-lg-none d-flex align-items-center">
            <span onClick={handleShowSideBar}>
              <MenuIcon />
            </span>
          </li>
        </ul>
      </nav>

      {/* sidebaer  */}
      <div className="sidebar d-lg-none d-block" style={{ width: "100%" }}>
        <nav ref={sidebarRef} className="container">
          <ul>
            {links.map((link) => (
              <li key={`s${link.title}`}>
                {!link.subs.length ? (
                  <Link to={link.path}>
                    <span
                      className="sidebar-item-title"
                      style={{ color: "var(--blue-darker)" }}
                    >
                      {link.title}
                    </span>
                  </Link>
                ) : (
                  <article>
                    <div
                      className="d-flex justify-content-between"
                      style={{ color: "var(--blue-darker)" }}
                      onClick={handleShowSideBarSubMenu}
                    >
                      <span className="sidebar-item-title flex-grow-1">
                        {link.title}
                      </span>
                      <span className="icon">{">"}</span>
                    </div>
                    <div className="sidebar-submenu">
                      <ul>
                        {link.subs.map((subLink) => (
                          <li key={`s${subLink.title}`}>
                            <Link to={subLink.path}>
                              <span
                                className="px-3 sidebar-item-title"
                                style={{ color: "var(--blue-darker)" }}
                              >
                                {subLink.title}
                              </span>
                            </Link>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </article>
                )}
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
