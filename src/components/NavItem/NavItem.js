import { useEffect, useRef, useState } from "react";

import SubNavItem from "../SubNavItem/SubNavItem";
import ArrowIcon from "../Icon/ArrowIcon";
import "./NavItem.css";
import { Link } from "react-router-dom";

const NavItem = ({ children, with_sub, subs }) => {
  const [isText, setIsString] = useState(true);
  const navItemRef = useRef();

  useEffect(() => {
    setIsString(typeof children == "string");

    // handle event
    navItemRef.current.addEventListener("click", () => {
      const active = document.querySelector(".nav-item.active");
      active && active.classList.remove("active");
      navItemRef.current.classList.add("active");
    });
  }, [isText, children, with_sub]);

  return (
    <div className="nav-item  d-flex align-items-center h-100" ref={navItemRef}>
      {isText && <ArrowIcon />}
      <Link to="">
        <span className="fw-bold">{children}</span>
      </Link>

      {with_sub && (
        <ul className="drop-down-wrapper p-3 bg-white">
          {subs.map((link) => (
            <li key={link.title}>
              <Link to={link.path}>
                <SubNavItem children={link.title} />
              </Link>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default NavItem;
