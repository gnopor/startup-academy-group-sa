import { useRef, useEffect } from "react";

import ArrowIcon from "../Icon/ArrowIcon";
import "./SubNavItem.css";

const NavItem = ({ children }) => {
  const subNavItemRef = useRef();

  useEffect(() => {
    // handle event
    subNavItemRef.current.addEventListener("click", () => {
      const active = document.querySelector(".sub-nav-item.active");
      console.log(active);
      active && active.classList.remove("active");
      subNavItemRef.current.classList.add("active");
    });
  }, []);

  return (
    <span
      className="sub-nav-item d-flex align-items-center fw-bold py-2"
      ref={subNavItemRef}
    >
      <ArrowIcon />
      {children}
    </span>
  );
};

export default NavItem;
