import "./Address.css";

const Address = ({ city, location, phone_number }) => {
  return (
    <article className="d-flex flex-column align-items-center ">
      <h4 style={{ color: "#FFF" }} className="fw-bolder">
        {city}
      </h4>
      <div className="d-flex flex-column align-items-center my-4">
        <span className="pin-icon" style={{ fontFamily: "icomoon" }}>
          &#xea54;
        </span>
        <span className="text-center">{location}</span>
      </div>
      <a href={`tel:${phone_number.split(" ").join("")}`}>
        <span className="mx-2" style={{ fontFamily: "icomoon" }}>
          &#xea86;
        </span>
        +(237) {phone_number}
      </a>
    </article>
  );
};

export default Address;
