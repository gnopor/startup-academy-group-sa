import "./Button.css";

import ArrowIcon from "../Icon/ArrowIcon";

const Button = ({ children, background }) => {
  return (
    <div
      className="px-4 py-2 d-flex custom-button"
      style={{
        background,
        width: "fit-content",
        borderRadius: "10px",
        cursor: "pointer",
      }}
    >
      <div className="arrow-left">
        <ArrowIcon />
      </div>
      <button
        className="border-0 bg-transparent fw-bold"
        style={{ color: "#FFF" }}
      >
        {children}
      </button>
      <div className="arrow-right">
        <ArrowIcon />
      </div>
    </div>
  );
};

Button.defaultProps = {
  background: "var(--yellow)",
};

export default Button;
